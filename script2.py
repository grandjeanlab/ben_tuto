from utils.update_model import update_weight

# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# tf.Session(config=config)

input_folder = r'cross_domain/epi/src'
label_folder = ''  # No label required.
output_folder_base = r'cross_domain/epi'  # save pred nii files.(time id based on weight)
old_model_name = r'weight/unet_fp32_all_BN_NoCenterScale_polyic_epoch15_bottle256_04012051/'  # load and adjust BN base on this old model(fixed/source domain usually)
new_model_name = 'EPI-DA'  # The prefix of new weight filename will use this string
cvs_result_base = r'cross_domain/epi'  # evaluation metric in csv. (path of csv, time id)
need_rotate = True


# transfer BEN to new domain/dataset
new_weight = update_weight(input_folder, label_folder, need_mkdir=True, weight=old_model_name,
                need_rotate=need_rotate, model_name=new_model_name,
                BN_list=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1], freeze=True)
print('Finetune done.\n New model path is : ', new_weight)


# setting filename
import re

num_extract = re.findall(r'\d+', new_weight)
output_folder = output_folder_base + '/' + 'pred' + '-' + num_extract[0]  # e.g. 'pred-04061444'
DA_path = output_folder
cvs_result = cvs_result_base + '/' + new_weight.replace('weight/', '').replace('/', '') + '.csv'
new_weight = new_weight + '.hdf5'

from utils.inference import inference_pipeline
import tensorflow.keras.backend as K

K.clear_session()  # release GRAM

# run inference
inference_pipeline(input_folder,
          output_folder, is_mkdir=True,
          weight=new_weight,
          need_rotate=need_rotate,
          BN_list=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],

          )


#''' zero-shot '''
zeroshot_out_folder = output_folder_base + '/' + 'pred' + '-' + 'zeroshot'
inference_pipeline(input_folder,
          zeroshot_out_folder, is_mkdir=True,
          weight=old_model_name,  # zeroshot
          # weight=new_weight,
          need_rotate=need_rotate,
          BN_list=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],

          )

from glob import glob
from utils.load_data import get_itk_array
from utils.visualization import load_slice, plot_segmentation

raw, zeroshot, DA = load_slice(raw_path=r'cross_domain/epi/src',
                  zeroshot_path=r'cross_domain/epi/pred-zeroshot',
                  DA_path=DA_path,
                  scans_num=1)
plot_segmentation(raw, zeroshot, DA, task='modality', hspace=-0.5, figsize=(16,9))         